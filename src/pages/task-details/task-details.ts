import { Component } from '@angular/core';
import { NavController, NavParams,Events } from 'ionic-angular';
import {UtilityService} from '../../providers/utility-service';
import { Storage } from '@ionic/storage';
import {UserFeedback} from '../../utilities/user-feedback'
import {CreateTaskPage} from '../create-task/create-task'
import {DomSanitizer} from '@angular/platform-browser';

/*
  Generated class for the TaskDetails page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-task-details',
  templateUrl: 'task-details.html',
  providers:[UtilityService,UserFeedback]
})
export class TaskDetailsPage {
  task:any={title:'',desc:'',points:'',parent:'',parent_id:{avatar:"assets/icon.png"},notes:[]};
  user_id:string;
  task_id:string;
  user_name:string;
  user_avatar:string;
  child_avatar:string;
  note_txt:string;
  constructor(public _DomSanitizationService:DomSanitizer,private events:Events,private userFeedback:UserFeedback,public storage:Storage,public utilityService:UtilityService,public navCtrl: NavController, public navParams: NavParams) {
    this.task_id=navParams.get('task_id');
    this.child_avatar=navParams.get('child_avatar');
    storage.get("userInfo").then(user=>{
      this.user_id=user._id;
      this.user_avatar=user.avatar;
      this.user_name=user.first_name+" "+user.last_name;
      this.get_task_details(true);
    });

  }

  ngOnInit() {
    this.events.subscribe("updated_task",()=>{
      this.get_task_details();
    });

    this.events.subscribe("task_done",(msg)=>{
      this.get_task_details();
    });

    this.events.subscribe("task_approved",(msg)=>{
      this.get_task_details();
    });
    this.events.subscribe("update_task",(msg)=>{
      this.get_task_details();
    });

    this.events.subscribe("not_complete",(msg)=>{
      this.get_task_details();
    });

    this.events.subscribe("task_chat",(msg)=>{
      if(!this.task.notes)
        this.task.notes=[];
      //this.task.notes.reverse();
      this.task.notes.unshift({txt:msg.msg,user_id:msg.user_id});
    });
  }


  onPageWillLeave() {
    this.events.unsubscribe("updated_task");
    this.events.unsubscribe("task_done");
    this.events.unsubscribe("task_approved");
    this.events.unsubscribe("update_task");
    this.events.unsubscribe("not_complete");
    this.events.unsubscribe("task_chat");
  }

  get_task_details(loading=false){
    if(loading)
      this.userFeedback.start_progress();
    this.utilityService.getTaskDetails(this.task_id).then(taskDetails=>{
      if(loading)
        this.userFeedback.close_progress();
      this.task=taskDetails;
      this.task.notes.reverse();
      if(this.task.child==this.user_id)
        this.child_avatar=this.user_avatar;
      this.task.parent_id=this.task.parent;
      this.task.parent=this.task.parent.first_name+" "+this.task.parent.last_name;
      if(this.task.deadline){
        let deadline=new Date(this.task.deadline?this.task.deadline:this.task.next_time);
        let day=deadline.getUTCDate()
        let year=deadline.getUTCFullYear();
        let month = deadline.toLocaleString("en-us", { month: "long" });
        this.task.time=day+" "+month+" "+year;
        this.task.hours=this.formatAMPM(deadline);
      }
    }).catch(err=>{})
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad TaskDetailsPage');
  }

  formatAMPM(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0'+minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
  }


  task_done(){
    //this.userFeedback.start_progress();
    this.utilityService.task_done(this.user_id,this.task_id,this.user_name,this.task.title).then((res)=>{
      //this.userFeedback.close_progress();
      this.userFeedback.make_toast("Marked as done");
      this.events.publish("new_event",{
        event_name:"task_done",
        to:this.task.parent_id._id,
        username:this.user_name,
        task:this.task.title,
        task_id:this.task._id,
        avatar:this.user_avatar
      });
      this.task.waiting=true;
    });
    this.navCtrl.pop();
  }

  approve(){
    //this.userFeedback.start_progress();
    this.utilityService.approve(this.task_id,this.task.points,this.task.child,this.task.title).then((res)=>{
      //this.userFeedback.close_progress();
      //this.userFeedback.make_toast("task "+this.task.title+" was approved");
      this.task.status="closed";
      this.events.publish("new_event",{
        event_name:"task_approved",
        to:this.task.child,
        username:this.user_name,
        task:this.task.title,
        task_id:this.task._id,
      });
    });
    this.navCtrl.pop();
  }


  not_complete(){
    //this.userFeedback.start_progress();
    this.utilityService.not_complete(this.task_id,this.task.child,this.user_name,this.task.title).then((res)=>{
      //this.userFeedback.close_progress();
      //this.userFeedback.make_toast("task "+this.task.title+" not complete");
      this.task.waiting=false;
      this.events.publish("new_event",{
        event_name:"not_complete",
        to:this.task.child,
        username:this.user_name,
        task:this.task.title,
        task_id:this.task._id,
      });
    });
    this.navCtrl.pop();
  }

  delete_task(){
    this.userFeedback.presentConfirm(()=>{
      //this.userFeedback.start_progress();
      this.utilityService.delete_task(this.task._id).subscribe(result=>{
        //this.userFeedback.close_progress();
        this.userFeedback.make_toast(this.task.title+" was deleted");
        this.events.publish("new_event",{
          event_name:"task_delete",
          to:this.task.child,
          username:this.user_name,
          task:this.task.title
        });
      });
      this.navCtrl.pop();
    })
  }

  edit_task(){
    this.navCtrl.push(CreateTaskPage,{
      task:this.task
    });
  }

  add_note(){
    //this.userFeedback.start_progress();
    this.utilityService.add_note(this.note_txt,this.task._id,this.user_id).subscribe(result=>{
    this.events.publish('new_event',{
        event_name:"task_chat",
        to:this.user_id==this.task.child?this.task.parent_id._id:this.task.child,
        msg:this.note_txt,
        user_id:this.user_id,
        username:this.user_name,
        task:this.task.title,
        task_id:this.task_id,
        avatar:this.child_avatar
      });
      this.note_txt="";
      //this.userFeedback.close_progress();
    });
  }
}
