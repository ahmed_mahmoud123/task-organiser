import { Component } from '@angular/core';
import { NavController, NavParams,Events } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators,FormControl } from '@angular/forms';
import { EmailValidator } from  '../../validators/EmailValidator';
import { PhoneValidator } from  '../../validators/PhoneValidator';
import { Storage } from '@ionic/storage';
import {DashboardPage} from '../dashboard/dashboard';
import {AuthService} from '../../providers/auth-service';
import {UtilityService} from '../../providers/utility-service';
import {SocialLoginPage} from '../social-login/social-login';
import {Camera} from 'ionic-native';
import {DomSanitizer} from '@angular/platform-browser';
import {UserFeedback} from '../../utilities/user-feedback';
/*
  Generated class for the Profile page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',

  providers:[FormBuilder,AuthService,UserFeedback,UtilityService]
})
export class ProfilePage {
  base64Image:string='assets/images/avatar.jpg';
  profileform:FormGroup;
  phoneValidator:PhoneValidator;
  emailValidator:EmailValidator;
  cachedUserInfo:any;
  phone: FormControl;
  email: FormControl;
  counties:Array<any>;
  days:Array<number>=[];
  months:Array<number>=[];
  years:Array<number>=[];
  c_flag:string;
  constructor(private events:Events,private utilityService:UtilityService,private userFeedback:UserFeedback,public storage:Storage,public navCtrl: NavController, public navParams: NavParams,public authService:AuthService,public formBuilder: FormBuilder,private _DomSanitizationService: DomSanitizer) {
    this.phone=new FormControl("",Validators.compose([Validators.required,Validators.pattern('[0-9 ]*')]));
    this.email=new FormControl("",Validators.required);
    this.profileform=this.formBuilder.group({
        first_name:['',Validators.compose([Validators.required,Validators.pattern('[a-zA-Z ]*')])],
        last_name:['',Validators.compose([Validators.required,Validators.pattern('[a-zA-Z ]*')])],
        email:this.email,
        phone:this.phone,
        gender:['male'],
        country_code:[''],
        birth_day:['1'],
        birth_month:['1'],
        birth_year:['1990']
    });

    for (let i = 1; i <= 12; i++) {
        this.months.push(i);
    }
    for (let i = 1; i <= 31; i++) {
        this.days.push(i);
    }
    for (let i = new Date().getFullYear(); i >= new Date().getFullYear()-117; i--) {
        this.years.push(i);
    }
    storage.get('countries').then(counties=>{
      this.counties=counties
      this.profileform.controls['country_code'].setValue(counties[0].code);
    })

    storage.get("userInfo").then((user)=>{
      this.cachedUserInfo=user;
      this.phoneValidator=new PhoneValidator(authService,user._id);
      this.emailValidator=new EmailValidator(utilityService,user._id);
      this.phone.asyncValidator=this.phoneValidator.isPhoneExists;
      this.email.setValidators(this.emailValidator.isValid);
      this.email.asyncValidator=this.emailValidator.isEmailExistsForProfile;
      this.profileform.controls['first_name'].setValue(user.first_name);
      this.profileform.controls['phone'].setValue(user.phone);
      this.profileform.controls['last_name'].setValue(user.last_name);
      this.profileform.controls['gender'].setValue(user.gender);
      this.profileform.controls['email'].setValue(user.email);
      this.profileform.controls['birth_day'].setValue(user.birth_day);
      this.profileform.controls['birth_month'].setValue(user.birth_month);
      this.profileform.controls['birth_year'].setValue(user.birth_year);
      if(user.country_code)
        this.profileform.controls['country_code'].setValue(user.country_code);
      else
        this.profileform.controls['country_code'].setValue("+44");
      this.base64Image=user.avatar;
      this.get_flag(this.profileform.controls['country_code'].value);
    });


  }

  getAvatar(){
    Camera.getPicture({
      destinationType:Camera.DestinationType.DATA_URL,
      targetWidth:500,
      targetHeight:500,
      sourceType:0,
      correctOrientation:true
    }).catch((cancel)=>{}).then((imageData)=>{
        if(imageData)
          this.base64Image="data:image/jpeg;base64, "+imageData;
    },(error)=>{

    })
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
  }

  save(event:any){
    if(this.profileform.valid){
      this.userFeedback.start_progress();
      let user=this.profileform.value;
      user._id=this.cachedUserInfo._id;
      user.avatar=this.base64Image;
      this.authService.updateProfile(user).then(result=>{
        this.userFeedback.close_progress();
        if(user){
          this.userFeedback.make_toast("Account details updated");
          for(let property in user){
            this.cachedUserInfo[property]=user[property];
          }
          this.storage.set("userInfo",this.cachedUserInfo);
          this.navCtrl.setRoot(DashboardPage);
        }
      }).catch(error=>{})
    }
  }

  get_flag(code){
    for (let i = 0; i < this.counties.length; i++) {
        if(this.counties[i].code==`+${code}` || this.counties[i].code==code)
        {
          this.c_flag=this.counties[i].emoji
          break;
        }else{
          this.c_flag="";
        }
    }
  }

  check_date(){
    let user=this.profileform.value;
    let date=new Date(user.birth_month+"/"+(user.birth_day)+"/"+user.birth_year);
    if(date.toString() != 'Invalid Date'){
      this.profileform.controls['birth_day'].setValue(date.getDate());
      this.profileform.controls['birth_month'].setValue(date.getMonth()+1);
      this.profileform.controls['birth_year'].setValue(date.getFullYear());
    }
  }

  logout(){
    if(this.cachedUserInfo){
      this.authService.remove_token(this.cachedUserInfo._id).then(()=>{});
      this.events.publish("new_event",{
        event_name:"logout",
        user:this.cachedUserInfo._id
      });
    }
    this.storage.clear().then(()=>{
      this.navCtrl.setRoot(SocialLoginPage);
    });
  }

}
