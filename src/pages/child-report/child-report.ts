import { Component } from '@angular/core';
import { NavController, NavParams,Events } from 'ionic-angular';
import {TaskDetailsPage} from '../task-details/task-details';
import {UtilityService} from '../../providers/utility-service';
import {UserFeedback} from '../../utilities/user-feedback';
import {CreateTaskPage} from '../create-task/create-task'
import { Storage } from '@ionic/storage';
import {DomSanitizer} from '@angular/platform-browser';


@Component({
  templateUrl: 'child-report.html',
  providers:[UtilityService,UserFeedback]
})
export class ChildReportPage {
  child:any={avatar:"assets/icon.png"};
  tasks:Array<any>=[];
  status:string='open';
  user:any={};
  tasks_segment:string='open_tasks';
  child_avatar:string;
  child_sel:string;
  reportDate:string;
  constructor(private event:Events,public _DomSanitizationService:DomSanitizer,public userFeedback:UserFeedback,public storage:Storage,public utilityService:UtilityService,public navCtrl: NavController, public navParams: NavParams) {
    let now=new Date();
    let month=Number(now.getUTCMonth())<10?`0${now.getUTCMonth()+1}`:now.getUTCMonth()+1;
    this.reportDate=`${now.getUTCFullYear()}-${month}`;
    this.child_sel=navParams.get('child_id');
    this.child_avatar=navParams.get('avatar');
    this.get_user_report(true);

  }


  ngOnInit() {
    this.event.subscribe('task_approved',()=>{
      this.get_user_report();
    });

    this.event.subscribe("task_done",(msg)=>{
      this.get_user_report();
    });

    this.event.subscribe("task_delete",(msg)=>{
      this.get_user_report();
    });

    this.event.subscribe("create_task",(msg)=>{
      this.get_user_report();
    });

    this.event.subscribe("update_task",(msg)=>{
      this.get_user_report();
    });

    this.event.subscribe("not_complete",(msg)=>{
      this.get_user_report();
    });

    this.event.subscribe("new_event",(msg)=>{
      let events=["task_delete","task_done","task_approved","not_complete"].join(',')
      if(events.indexOf(msg.event_name))
        this.get_user_report();
    });
  }

  onPageWillLeave() {
    this.event.unsubscribe('task_approved');
    this.event.unsubscribe("task_done");
    this.event.unsubscribe("task_delete");
    this.event.unsubscribe("create_task");
    this.event.unsubscribe("update_task");
    this.event.unsubscribe("not_complete");
    this.event.unsubscribe("new_event");
  }
  get_user_report(loading=false){
      this.storage.get('userInfo').then(data=>{
        this.user=data;
        if(data._id==this.child_sel){
          this.child=data;
        }else{
          this.child=data.childeren.filter(child=>{
            return child.id._id==this.child_sel;
          }).map(child=>{
            return child.id;
          }).shift();
        }
        this.tasksStatusChange(this.status,loading);
      });
  }

  tasksStatusChange(status,loading){
    this.status=status;
    if(this.child._id){
      if(loading)
        this.userFeedback.start_progress();
      this.utilityService.getTasks(this.child._id,this.user._id,status,this.reportDate).subscribe((tasks:Array<any>)=>{
        if(loading)
          this.userFeedback.close_progress();
        this.tasks=tasks;
        this.tasks.reverse();
      },(err)=>{});
    }
  }

  taskDetails(task_id,child_avatar){
    this.navCtrl.push(TaskDetailsPage,{
      task_id:task_id,
      child_avatar:child_avatar
    });
  }

  createTask(){
    this.navCtrl.push(CreateTaskPage,{
      child_sel:this.child_sel
    });
  }

  filter(){
    this.get_user_report();
  }

}
