import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { SocialSharing } from '@ionic-native/social-sharing';
import {UserFeedback} from '../../utilities/user-feedback';

/*
  Generated class for the Invite page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-invite',
  templateUrl: 'invite.html',
  providers:[SocialSharing,UserFeedback]
})
export class InvitePage {

  messages:string="I'm loving Task Organiser to share, allocate and manage my tasks. Try it out at: http://www.taskorganiser.co.uk/ I highly recommend!";
  title:string="Task Organiser";
  image:any="";
  url:string="";
  constructor(private userFeedback:UserFeedback,private socialSharing:SocialSharing,public navCtrl: NavController, public navParams: NavParams) {
    userFeedback.convertToDataURLviaCanvas("assets/images/invite_image.jpg", "image/jpeg")
    .then( base64Img =>
      {
        this.image=base64Img;
        this.socialSharing.share(this.messages,this.title,this.image,this.url).then(rs=>{});
      })
  }
  ionViewDidLoad() {
  }


  share_facebook(){
    this.socialSharing.shareViaFacebookWithPasteMessageHint("test message",this.image,this.url,this.messages).then(result=>{
    },err=>{
      this.userFeedback.alert("Fail","can not share using Facebook");
    }).catch(err=>{
      this.userFeedback.alert("Fail","can not share using Facebook");
    })
  }


  share_twitter(){
    this.socialSharing.shareViaTwitter(this.messages,this.image,this.url).then(result=>{
    },err=>{
      this.userFeedback.alert("Fail","can not share using Twitter");
    }).catch(err=>{
      this.userFeedback.alert("Fail","can not share using Twitter");
    })
  }

  share_instgram(){
    this.socialSharing.shareViaInstagram(this.messages,this.image).then(result=>{
    },err=>{
      this.userFeedback.alert("Fail","can not share using Instagram");
    }).catch(err=>{
      this.userFeedback.alert("Fail","can not share using Instagram");
    })
  }

  share_sms(){
    this.socialSharing.shareViaSMS(this.messages,"").then(result=>{
    },err=>{
      this.userFeedback.alert("Fail","can not share using SMS");
    }).catch(err=>{
      this.userFeedback.alert("Fail","can not share using SMS");
    })
  }

  share_whatsapp(){
    this.socialSharing.shareViaWhatsApp(this.messages,this.image,this.url).then(result=>{
    },err=>{
      this.userFeedback.alert("Fail","can not share using What's app");
    }).catch(err=>{
      this.userFeedback.alert("Fail","can not share using  What's app");
    })
  }

  share_email(){
    this.socialSharing.shareViaEmail(this.messages,this.title,[""]).then(result=>{
    },err=>{
      this.userFeedback.alert("Fail","can not share using Email");
    }).catch(err=>{
      this.userFeedback.alert("Fail","can not share using Email");
    })
  }

}
