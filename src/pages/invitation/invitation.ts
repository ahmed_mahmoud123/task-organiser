import { Component } from '@angular/core';
import { NavController, NavParams,Events } from 'ionic-angular';
import {DomSanitizer} from '@angular/platform-browser';
import {UserFeedback} from '../../utilities/user-feedback';
import {UtilityService} from '../../providers/utility-service';
import {DashboardPage} from '../dashboard/dashboard';
import {Storage} from '@ionic/storage';

/*
  Generated class for the Invitation page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-invitation',
  templateUrl: 'invitation.html',
  providers:[UserFeedback,UtilityService]
})
export class InvitationPage {
  invitations:Array<any>=[];
  user:any;
  constructor(private storage:Storage,private events:Events,private utilityService:UtilityService,private userFeedback:UserFeedback,private _DomSanitizationService: DomSanitizer,public navCtrl: NavController, public navParams: NavParams) {
    this.invitations=navParams.get("invitations");
    storage.get("userInfo").then(user=>{
      this.user=user;
    })
    if(!this.invitations){
      let user_id=navParams.get("user_id");
      userFeedback.start_progress();
      utilityService.get_invitations(user_id).then((invitations:any)=>{
          userFeedback.loader.dismiss(err=>{})
          this.invitations=invitations;
      }).catch(err=>{userFeedback.loader.dismiss(err=>{})});
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad InvitationPage');
  }

  performInvitation(invitation_id,type,sender_id){
    this.userFeedback.start_progress();
    this.utilityService.performInvitation(invitation_id,type).subscribe(result=>{
      this.userFeedback.close_progress();
      let msg=type==1?"accepted":"rejected";
      if(msg=="accepted"){
        this.events.publish('new_event',{event_name:"accept_invitation",to:sender_id,username:this.user.first_name+" "+this.user.last_name})
      }else{
        this.events.publish('new_event',{event_name:"declined_invitation",to:sender_id,username:this.user.first_name+" "+this.user.last_name})
      }
    },(err)=>{this.userFeedback.close_progress();});
    for (let i = 0; i < this.invitations.length; i++) {
        if(this.invitations[i]._id==invitation_id){
          this.invitations.splice(i,1);
          break;
        }
    }
    if(!this.invitations.length)
      this.navCtrl.setRoot(DashboardPage,{no_check:true});
  }
}
