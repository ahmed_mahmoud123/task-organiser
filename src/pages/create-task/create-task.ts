import { Component } from '@angular/core';
import { NavController, NavParams,Events } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {UtilityService} from '../../providers/utility-service';
import { Storage } from '@ionic/storage';
import { DatePicker } from '@ionic-native/date-picker';
import {UserFeedback} from '../../utilities/user-feedback';
import {Task} from '../../Models/Task';
import { RecurringTaskPage } from  '../recurring-task/recurring-task'
import {DashboardPage} from '../dashboard/dashboard'

/*
  Generated class for the CreateTask page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  templateUrl: 'create-task.html',
  providers:[FormBuilder,UtilityService,DatePicker,UserFeedback]
})
export class CreateTaskPage {
  childern:any;
  createTaskForm:FormGroup;
  id:String='0';
  deadline:number=+new Date();
  operation:string="Create";
  username:string;
  task:Task=new Task();
  custom_points:boolean=false;
  yearMonth:string;
  child_sel:string;
  constructor(private events:Events,public userFeedback:UserFeedback,private datePicker: DatePicker,public utilityService:UtilityService,public storage:Storage,public formBuilder:FormBuilder,public navCtrl: NavController, public navParams: NavParams,public event:Events) {
    let now=new Date();
    let month=Number(now.getUTCMonth())<10?`0${now.getUTCMonth()+1}`:now.getUTCMonth()+1;
    this.yearMonth=`${now.getUTCFullYear()}-${month}`;

    let task=navParams.get('task');
    this.child_sel=navParams.get('child_sel');

    if(task){
      this.task=task;
      this.operation="Update"
      this.deadline=this.task.deadline?this.task.deadline:this.task.next_time;
      this.custom_points=true;
      [0,1,5,10,15].forEach(number=>{
        if(task.points==number)
          this.custom_points=false;
      })
    }
    this.createTaskForm=formBuilder.group({
      'title':[this.task.title,Validators.required],
      'desc':[this.task.desc],
      'child':[this.task.child,Validators.required],
      'points':[this.task.points,Validators.compose([Validators.required,Validators.pattern("[0-9]+")])]
    });
  }


  ionViewDidLoad() {
    let self=this;
    this.storage.get('userInfo').then(data=>{
      self.id=data._id;
      self.username=data.first_name+" "+data.last_name;
      this.childern=data.childeren.map(child=>{
        return child.id;
      });
      this.childern.push({
        first_name:data.first_name,
        _id:data._id
      })
      let child_id=this.childern[0]?this.childern[0]._id:undefined;
      setTimeout(()=>
      {
        if(self.task._id)
          child_id=self.task.child;
        if(this.child_sel)
          child_id=this.child_sel;
        self.createTaskForm.controls['child'].setValue(child_id);
      },500);
    })
  }
  do_task(with_deadline=1){
    if(this.createTaskForm.valid){
      this.userFeedback.loader.present().catch(err=>{});
      let task=this.createTaskForm.value;
      if(with_deadline)task.deadline=this.deadline;
      else delete task.deadline;
      if(!this.task._id){
        task.parent=this.id;
        this.create_task(task);
      }else{
        task._id=this.task._id;
        task.waiting=false;
        this.update_task(task);
      }
    }
  }

  create_task(task){
    task.yearMonth=this.yearMonth;
    this.utilityService.createTask(task).then((res:any)=>{
      this.userFeedback.loader.dismiss().catch(err=>{});
      this.events.publish("new_event",{
        event_name:"create_task",
        to:task.child,
        username:this.username,
        task:task.title,
        sender_id:this.id
      });
      if(res._id){
        this.task._id=res._id;
        this.userFeedback.make_toast("Task created");
        this.navCtrl.setRoot(DashboardPage)
      }
    },()=>{
      this.userFeedback.loader.dismiss().catch(err=>{});
    }).catch(err=>{this.userFeedback.loader.dismiss().catch(err=>{});})
  }
  update_task(task){
    this.utilityService.updateTask(task).then((res:any)=>{
      this.userFeedback.loader.dismiss().catch(err=>{});
      this.events.publish("new_event",{
        event_name:"update_task",
        to:task.child,
        username:this.username,
        task:task.title
      });
      this.userFeedback.make_toast("Task updated");
      this.navCtrl.pop();
    },()=>{
      this.userFeedback.loader.dismiss().catch(err=>{});
    }).catch(err=>{this.userFeedback.loader.dismiss().catch(err=>{});})
  }

  setDeadline(){
    this.datePicker.show({
      date: new Date(this.deadline),
      mode: 'datetime',
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_LIGHT
    }).then(
      date => {
        this.deadline=+date;
        this.do_task();
      },
      err => {}
    );
  }

  pointsChange(event){
    if(this.createTaskForm.value.points=='custom')
      {
        this.custom_points=true;
        this.createTaskForm.controls['points'].setValue(0);
      }
  }

  recurring_task(){
    let task:any;
    if(this.createTaskForm.valid){
      task=this.createTaskForm.value;
      task.yearMonth=this.yearMonth;
      task.deadline=this.deadline;
      if(!this.task._id){
        task.parent=this.id;
      }else{
        task._id=this.task._id;
        task.waiting=false;
      }
    }
    this.navCtrl.push(RecurringTaskPage,{
      task:task
    })
  }

}
