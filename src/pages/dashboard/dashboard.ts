import { Component } from '@angular/core';
import { MenuController,NavController, NavParams,Events } from 'ionic-angular';
import {CreateTaskPage} from '../create-task/create-task';
import {AddChildPage} from '../add-child/add-child';
import {TaskDetailsPage} from '../task-details/task-details';
import {InvitationPage} from '../invitation/invitation';

import {ChildReportPage} from '../child-report/child-report';

import { Storage } from '@ionic/storage';
import { UtilityService } from '../../providers/utility-service';
import { UserFeedback } from '../../utilities/user-feedback';

import {DomSanitizer} from '@angular/platform-browser';
import { BrowserTab } from '@ionic-native/browser-tab';


import {
  Push,
  PushToken
} from '@ionic/cloud-angular';

/*
  Generated class for the Dashboard page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  templateUrl: 'dashboard.html',
  providers:[UtilityService,BrowserTab]
})
export class DashboardPage {
  personal_childern:Array<any>=[];
  work_childern:Array<any>=[];
  user:any;
  points:number=0;
  current_adv:string;
  current_adv_link:string;
  index:number=1;
  data:any;
  constructor(private browserTab:BrowserTab,private userFeedback:UserFeedback,private utilityService:UtilityService,public push:Push,public storage:Storage,private _DomSanitizationService: DomSanitizer,public navCtrl: NavController, public navParams: NavParams,public event:Events,menuCtrl:MenuController) {
    let no_check=navParams.get("no_check");
    this.get_adverts();
    menuCtrl.enable(true,"left");
    storage.get('userInfo').then(data=>{
      this.user=data;
      if(!no_check)
        this.check_invitations();
      if(!data.dev_token)
        this.get_send_user_token();
      setTimeout(()=>{this.get_childern()},3000);
    }).catch(err=>{});
  }

  check_invitations(){
    this.utilityService.get_invitations(this.user._id).then((invitations:any)=>{
        if(invitations.length>0)
        this.navCtrl.setRoot(InvitationPage,{
          invitations:invitations
        });
    })
  }

  ngOnInit() {
    this.event.subscribe('task_approved',(data)=>{
      this.get_childern();
    });

    this.event.subscribe('accept_invitation',(data)=>{
      this.userFeedback.generate_notification(1, data.username+" accepted your invitation");
      this.get_childern();
    });
  }

  onPageWillLeave() {
    this.event.unsubscribe('task_approved');
    this.event.unsubscribe('accept_invitation');
  }

  get_childern(refresher=undefined){
    this.utilityService.getChildern(this.user._id).then((rs:any)=>{
      if(refresher)
        refresher.complete();
      this.personal_childern=rs.childeren.filter(child=>{
        return child.type=="personal";
      }).map(child=>{
          return child.id;
      });
      this.personal_childern.unshift(this.user);
      this.work_childern=rs.childeren.filter(child=>{
        return child.type=="work";
      }).map(child=>{
          return child.id;
      });
      this.user.childeren=rs.childeren;
      this.storage.set('userInfo',this.user);
    }).catch(err=>{});
  }

  get_points(){
    this.utilityService.getPoints(this.user._id).then((rs:any)=>{
      this.points=rs.points;
      this.data.points=this.points;
      this.storage.set('userInfo',this.data);
    })
  }
  get_adverts(){
    this.utilityService.get_adverts().subscribe(adverts=>{
      this.current_adv="data:image/png;base64,"+adverts[0].data;
      this.current_adv_link=adverts[0].advert_link;
      setInterval((self,length)=>{
        if(self.index==length)
          self.index=0;
        this.current_adv="data:image/png;base64,"+adverts[self.index].data
        this.current_adv_link=adverts[self.index].advert_link;
        self.index++;
      },10000,this,adverts.length)
    });
  }
  get_send_user_token(){
    this.push.register().then((t: PushToken) => {
      this.utilityService.update_token(t.token,this.user._id);
    })
    this.push.rx.notification()
    .subscribe((msg:any) => {
      this.get_childern();
      this.get_points();
    });
  }


  createTask(){
    this.navCtrl.push(CreateTaskPage);
  }

  addChild(){
    this.navCtrl.push(AddChildPage);
  }

  childReport(child_id,avatar){
    this.navCtrl.push(ChildReportPage,{
      child_id:child_id,
      avatar:avatar
    });
  }

  open_url(link){
    this.browserTab.openUrl(link);
  }

  doRefresh(event){
    this.get_childern(event);
  }

}
