import { Component } from '@angular/core';
import { NavController, NavParams,Events} from 'ionic-angular';
import { FormBuilder, FormGroup } from '@angular/forms';
import { EmailValidator } from  '../../validators/EmailValidator';
import { UtilityService } from '../../providers/utility-service';
import { Storage } from '@ionic/storage';
import {UserFeedback} from '../../utilities/user-feedback';
import { Contacts, Contact, ContactField, ContactName } from '@ionic-native/contacts';

/*
  Generated class for the AddChild page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-add-child',
  templateUrl: 'add-child.html',
  providers:[FormBuilder,UtilityService,UserFeedback,Contacts]
})
export class AddChildPage {
  addChildForm:FormGroup;
  emailValidator:EmailValidator;
  loader:any;
  contactsList:Array<any>=[];
  user:any;
  childern_ids:string;
  childern_emails:string;


  constructor(public contacts:Contacts,private events:Events,public userFeedback:UserFeedback,public storage:Storage,public utilityService:UtilityService,public navCtrl: NavController, public navParams: NavParams,public formBuilder:FormBuilder) {
    this.emailValidator=new EmailValidator(utilityService,0);
    this.addChildForm=formBuilder.group({
      child_email:['',this.emailValidator.isValid,this.emailValidator.isEmailExists],
      relation_type:['-']
    })

    this.storage.get('userInfo').then(user=>{
      this.user=user;
      this.childern_ids=user.childeren.map((childern)=>{return childern.id._id}).join(",");
      this.childern_emails=user.childeren.map((childern)=>{return childern.id.email}).join(",");
      this.childern_emails+=user.email;
      this.get_contacts_accounts();
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddChildPage');
  }

  addChild(){
    if(this.addChildForm.valid){
      let formValues=this.addChildForm.value;
      this.add_child(this.user.email,formValues.child_email);
    }
  }


  add_child(parent,child){
    let self=this;
    if(this.addChildForm.value.relation_type === "-"){
      self.userFeedback.make_toast("kindly select a relationship type.");
    }else{
      if(this.childern_ids.indexOf(child) >= 0 || this.childern_emails.indexOf(child) >=0){
        self.userFeedback.make_toast("not allowed , already added");
        return;
      }
      this.userFeedback.start_progress();
      this.utilityService.addChild(parent,child,self.user._id,this.addChildForm.value.relation_type,this.user.first_name+" "+this.user.last_name).then((result:any)=>{
        self.userFeedback.close_progress();
        if(result.ok==-1){
          self.userFeedback.make_toast("An invitation was sent before, waiting response");
        }else{
          self.userFeedback.make_toast("Add request sent");
          this.events.publish("new_event",{
            event_name:"new_invitation",
            to:child
          });
        }
      },(err)=>{}).catch(err=>{})
    }
  }

  get_contacts_accounts(refresher=undefined){
    this.userFeedback.start_progress();
    let self=this;
    this.contacts.find(["name"]).then((contact)=>{
      var contactNumber=contact.map((contact)=>{
        if(contact.phoneNumbers){
            let numbers=[];
            contact.phoneNumbers.forEach((number)=>{
                let value=number.value.match(/\d/g).join("");
                if(value.length>6)
                  value=value.substring(value.length-8);
                numbers.push(value);
            });
            return numbers.join(",");
        }
      });
      contactNumber=contactNumber.filter(function(val) { return val != null; });
      this.utilityService.get_friends_accounts(contactNumber,self.user._id).then((contacts:Array<any>)=>{
        this.userFeedback.close_progress();
        if(refresher)
          refresher.complete();
        let self=this;
        this.contactsList=contacts.filter((contact)=>{ return self.childern_ids.indexOf(contact._id) < 0});
      }).catch(err=>{this.userFeedback.close_progress()});
    })
  }

  add_contact(contact){
    this.add_child(this.user.email,contact);
    for (let i = 0; i < this.contactsList.length; i++) {
        if(this.contactsList[i]._id==contact){
          this.contactsList.splice(i,1);
          break;
        }
    }
  }

  doRefresh(refresher){
    this.get_contacts_accounts(refresher);
  }
}
