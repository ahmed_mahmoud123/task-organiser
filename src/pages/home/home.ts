import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {UserFeedback} from '../../utilities/user-feedback';
import {ProfilePage} from '../profile/profile';
import { FormBuilder, FormGroup } from '@angular/forms';
import { EmailValidator } from  '../../validators/EmailValidator';
import { UtilityService } from '../../providers/utility-service';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers:[UserFeedback,UtilityService]
})
export class HomePage {
  resetForm:FormGroup;
  emailValidator:EmailValidator;
  constructor(private utilityService:UtilityService,private formBuilder:FormBuilder,private userFeedback:UserFeedback,public navCtrl: NavController, public navParams: NavParams) {
    this.emailValidator=new EmailValidator(utilityService,0);
    this.resetForm=formBuilder.group({
      email:['',this.emailValidator.isValid,this.emailValidator.isEmailExists],
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');
  }

  reset(){
    this.userFeedback.loader.present();
    this.utilityService.reset_password(this.resetForm.value.email).then(result=>{
      this.userFeedback.loader.dismiss();
      this.userFeedback.make_toast("Reset password email sent");
    },err=>this.userFeedback.loader.dismiss())
    .catch(err=>this.userFeedback.loader.dismiss())
  }

}
