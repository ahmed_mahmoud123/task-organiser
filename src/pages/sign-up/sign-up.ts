import { Component } from '@angular/core';
import { NavController, NavParams,Events } from 'ionic-angular';
import {DashboardPage} from '../dashboard/dashboard';
import {AuthService} from '../../providers/auth-service';
import {Camera} from 'ionic-native';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EmailValidator } from  '../../validators/EmailValidator';
import { PhoneValidator } from  '../../validators/PhoneValidator';
import {UtilityService} from '../../providers/utility-service';
import {UserFeedback} from '../../utilities/user-feedback';
import { Storage } from '@ionic/storage';
import {DomSanitizer} from '@angular/platform-browser';


/*
  Generated class for the SignUp page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-sign-up',
  templateUrl: 'sign-up.html',
  providers:[AuthService,FormBuilder,UtilityService,UserFeedback]

})
export class SignUpPage {
  base64Image:string='assets/images/avatar.jpg';
  singupform:FormGroup;
  type:String;
  phoneValidator:PhoneValidator;
  emailValidator:EmailValidator;
  dev_token:string;
  counties:Array<any>;
  days:Array<number>=[];
  months:Array<number>=[];
  years:Array<number>=[];
  c_flag:string;
  constructor(private event:Events,private userFeedback:UserFeedback,private utilityService:UtilityService,public storage:Storage,public navCtrl: NavController, public navParams: NavParams,public authService:AuthService,public formBuilder: FormBuilder,private _DomSanitizationService: DomSanitizer) {

    this.phoneValidator=new PhoneValidator(authService,0);
    this.emailValidator=new EmailValidator(utilityService,0);

    this.singupform=this.formBuilder.group({
        first_name:['',Validators.compose([Validators.required,Validators.pattern('[a-zA-Z ]*')])],
        last_name:['',Validators.compose([Validators.required,Validators.pattern('[a-zA-Z ]*')])],
        email:['',Validators.compose([Validators.required,this.emailValidator.isValid]),this.emailValidator.isEmailExistsForProfile],
        phone:['',Validators.required,this.phoneValidator.isPhoneExists],
        password:['',Validators.compose([Validators.required,Validators.minLength(4)])],
        gender:['male'],
        country_code:[''],
        birth_day:['1'],
        birth_month:['1'],
        birth_year:['2017']
    })
    for (let i = 1; i <= 12; i++) {
        this.months.push(i);
    }
    for (let i = 1; i <= 31; i++) {
        this.days.push(i);
    }
    for (let i = new Date().getFullYear(); i >= new Date().getFullYear()-117; i--) {
        this.years.push(i);
    }
    storage.get('countries').then(counties=>{
      this.counties=counties
      this.singupform.controls['country_code'].setValue("+44");
      this.get_flag("+44");
    })
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad SignUpPage');
  }

  getAvatar(){
    Camera.getPicture({
      destinationType:Camera.DestinationType.DATA_URL,
      targetWidth:500,
      targetHeight:500,
      sourceType:0,
      correctOrientation:true
    }).then((imageData)=>{
        this.base64Image="data:image/jpeg;base64, "+imageData;
    },(error)=>{}).catch((cancel)=>{
      //alert(cancel);
    })
  }

  register(){
    if(this.singupform.valid){
      this.userFeedback.loader.present();
        let user=this.singupform.value;
        user.avatar=this.base64Image;
        user.type=this.type;
        user.dev_token=this.dev_token;
        this.authService.createAccount(user).then((data)=>{
          this.userFeedback.loader.dismiss();
          this.storage.set("userInfo",data).then(()=>{
            this.event.publish("afterLogin",data);
            this.navCtrl.setRoot(DashboardPage);
          })
        },(error)=>{
          alert(error);
        });
    }
  }

  get_flag(code){
    for (let i = 0; i < this.counties.length; i++) {
        if(this.counties[i].code==`+${code}` || this.counties[i].code==code)
        {
          this.c_flag=this.counties[i].emoji
          break;
        }else{
          this.c_flag="";
        }
    }
  }

  check_date(){
    let user=this.singupform.value;
    let date=new Date(user.birth_month+"/"+(user.birth_day)+"/"+user.birth_year);
    this.singupform.controls['birth_day'].setValue(date.getDate());
    this.singupform.controls['birth_month'].setValue(date.getMonth()+1);
    this.singupform.controls['birth_year'].setValue(date.getFullYear());
  }
}
