import { CloudSettings, CloudModule } from '@ionic/cloud-angular';

const cloudSettings: CloudSettings = {
  'core': {
    'app_id': '79c098b1'
  },
  'push': {
    'sender_id': '559742935925',
    'pluginConfig': {
      'ios': {
        'badge': true,
        'sound': true
      },
      'android': {
        'iconColor': '#ccc'
      }
    }
  }
};
import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import {HomePage} from '../pages/home/home'
import {SocialLoginPage} from '../pages/social-login/social-login';
import {SignUpPage} from '../pages/sign-up/sign-up';
import {DashboardPage} from '../pages/dashboard/dashboard';
import {ChildReportPage} from '../pages/child-report/child-report';
import {CreateTaskPage} from '../pages/create-task/create-task';
import {TaskDetailsPage} from '../pages/task-details/task-details';
import {AddChildPage} from '../pages/add-child/add-child';
import {ProfilePage} from '../pages/profile/profile';
import {InvitationPage} from '../pages/invitation/invitation';
import {RecurringTaskPage} from '../pages/recurring-task/recurring-task';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { IonicStorageModule } from '@ionic/storage';
import {InvitePage} from '../pages/invite/invite';
import {SettingPage} from '../pages/setting/setting';
import { UtilityService } from '../providers/utility-service';

@NgModule({
  declarations: [
    ProfilePage,
    InvitePage,
    MyApp,
    HomePage,
    SocialLoginPage,
    SignUpPage,
    DashboardPage,
    ChildReportPage,
    CreateTaskPage,
    TaskDetailsPage,
    AddChildPage,
    InvitationPage,
    RecurringTaskPage,
    SettingPage
  ],
  imports: [
    IonicModule.forRoot(MyApp,{
      tabsPlacement: 'top',
      tabSubPages:false
    }),
    IonicStorageModule.forRoot(),
    CloudModule.forRoot(cloudSettings)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    ProfilePage,
    MyApp,
    HomePage,
    SocialLoginPage,
    InvitePage,
    SignUpPage,
    DashboardPage,
    ChildReportPage,
    CreateTaskPage,
    TaskDetailsPage,
    AddChildPage,
    InvitationPage,
    RecurringTaskPage,
    SettingPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    LocalNotifications,
    UtilityService,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
