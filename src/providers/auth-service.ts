import { Injectable } from '@angular/core';
import { Http,Headers } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the AuthService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class AuthService {
  headers:Headers;
  constructor(public http: Http) {
    console.log('Hello AuthService Provider');
    this.headers = new Headers ({ 'Content-Type': 'application/json' });
  }

  createAccount(user,account_type='register'){
    return new Promise(resolve=>{
      this.http.post("http://97.74.4.85:3000/users/"+account_type,JSON.stringify(user),{headers:this.headers}).map(resp=>resp.json())
      .subscribe(data=>{
        resolve(data);
      },(error)=>{
        alert(JSON.stringify(error))
        resolve(error);
      });
    })
  }

  login(user){
      return this.http.post("http://97.74.4.85:3000/users/login",JSON.stringify(user),{headers:this.headers}).map(resp=>resp.json()).toPromise();
  }
  getGoogleInfo(user_id){
    return new Promise(resolve=>{
      let API_KEY="AIzaSyBSSNIAS6P1q-KhbBnU45OrCqV6ncbOeWc";
      this.http.get('https://www.googleapis.com/plus/v1/people/'+user_id+'?key='+API_KEY).map(resp=>resp.json())
      .subscribe(data=>{
        resolve(data);
      },(error)=>{
        alert(JSON.stringify(error))
        resolve(error);
      });
    })
  }

  updateProfile(user){
    return new Promise(resolve=>{
      this.http.post("http://97.74.4.85:3000/users/update/",JSON.stringify(user),{headers:this.headers}).map(resp=>resp.json())
      .subscribe(data=>{
        resolve(data);
      },(error)=>{
        resolve(error);
      });
    })
  }

  delete_account(user_id){
    return this.http.get("http://97.74.4.85:3000/users/delete/"+user_id).map(resp=>resp.json()).toPromise();
  }

  remove_token(user_id){
    return this.http.get("http://97.74.4.85:3000/users/remove_token/"+user_id).map(resp=>resp.json()).toPromise();
  }
}
