import { Injectable } from '@angular/core';
import { Http,Headers } from '@angular/http';
import 'rxjs/add/operator/map';



@Injectable()
export class UtilityService {
  headers:Headers;
  api_url:string;
  constructor(public http: Http) {
    this.headers = new Headers ({ 'Content-Type': 'application/json' });
  }

  addChild(parent,child,parent_id,relation_type,username){
      return this.http.get("http://97.74.4.85:3000/invitations/add/"+parent+"/"+child+"/"+parent_id+"/"+relation_type+"/"+username).map(resp=>resp.json()).toPromise();
  }

  get_invitations(child_id){
    return new Promise(resolve=>{
      this.http.get("http://97.74.4.85:3000/invitations/get/"+child_id).map(resp=>resp.json()).subscribe(data=>{
        resolve(data);
      });
    })
  }

  
  performInvitation(invitation_id,code){
    return this.http.get("http://97.74.4.85:3000/invitations/perform/"+invitation_id+"/"+code).map(resp=>resp.json());
  }
  delete_task(task_id){
    return this.http.get("http://97.74.4.85:3000/tasks/delete/"+task_id);
  }

  getChildern(parent){
    return new Promise(resolve=>{
      this.http.get("http://97.74.4.85:3000/users/get_childern/"+parent).map(resp=>resp.json()).subscribe(data=>{
        resolve(data);
      });
    })
  }

  getPoints(user_id){
    return new Promise(resolve=>{
      this.http.get("http://97.74.4.85:3000/users/get_points/"+user_id).map(resp=>resp.json()).subscribe(data=>{
        resolve(data);
      });
    })
  }


  createTask(task){
    return new Promise(resolve=>{
      this.http.post("http://97.74.4.85:3000/tasks/create",JSON.stringify(task),{headers:this.headers}).map(resp=>resp.json()).subscribe(data=>{
        resolve(data);
      });
    })
  }

  recurringTask(task){
      return this.http.post("http://97.74.4.85:3000/tasks/recurring",JSON.stringify(task),{headers:this.headers}).map(resp=>resp.json());
  }

  updateTask(task){
    return new Promise(resolve=>{
      this.http.post("http://97.74.4.85:3000/tasks/update",JSON.stringify(task),{headers:this.headers}).map(resp=>resp.json()).subscribe(data=>{
        resolve(data);
      });
    })
  }

  getTasks(child,parent,status,yearMonth){
      return this.http.get("http://97.74.4.85:3000/tasks/get/"+child+"/"+parent+"/"+status+"/"+yearMonth).map(resp=>resp.json());
  }


  getTaskDetails(task_id){
    return new Promise(resolve=>{
      this.http.get("http://97.74.4.85:3000/tasks/details/"+task_id).map(resp=>resp.json()).subscribe(data=>{
        resolve(data);
      });
    })
  }

  task_done(child_id,task_id,child,task){
    return new Promise(resolve=>{
      this.http.get("http://97.74.4.85:3000/tasks/done/"+child_id+"/"+task_id+"/"+child+"/"+task).map(resp=>resp.json()).subscribe(data=>{
        resolve(data);
      });
    })
  }

  approve(task_id,points,user_id,title){
    return new Promise(resolve=>{
      this.http.get("http://97.74.4.85:3000/tasks/approve/"+task_id+"/"+points+"/"+user_id+"/"+title).map(resp=>resp.json()).subscribe(data=>{
        resolve(data);
      });
    })
  }

  not_complete(task_id,child_id,parent_name,title){
    return this.http.get(`http://97.74.4.85:3000/tasks/not_complete/${task_id}/${child_id}/${parent_name}/${title}`).map(resp=>resp.json()).toPromise();
  }

  get_friends_accounts(numbers,user_id){
    return new Promise(resolve=>{
      this.http.post("http://97.74.4.85:3000/users/accounts/",JSON.stringify({numbers:numbers,id:user_id}),{headers:this.headers}).map(resp=>resp.json()).subscribe(data=>{
        resolve(data);
      });
    })
  }

  get_adverts(){
    return this.http.get("http://97.74.4.85:3000/portal/adverts_json").map(resp=>resp.json());
  }

  get_countries(){
    return this.http.get("http://97.74.4.85:3000/portal/country").map(resp=>resp.json());
  }


  update_token(token,user_id){
     this.http.get("http://97.74.4.85:3000/users/token/"+user_id+"/"+token).subscribe(data=>{
     });
  }

  add_note(note,task_id,user_id){
      return this.http.post("http://97.74.4.85:3000/tasks/add_note",JSON.stringify({
        note:note,
        task_id:task_id,
        user_id:user_id
      }),{headers:this.headers});
  }

  reset_password(email){
    return this.http.get("http://97.74.4.85:3000/users/reset_password/"+email).map(resp=>resp.json()).toPromise();
  }


  get_task_by_parent(parent_id,child_id){
    return this.http.get(`http://97.74.4.85:3000/tasks/get_task_by_parent/${parent_id}/${child_id}`).map(resp=>resp.json()).toPromise();
  }
}
